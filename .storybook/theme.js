import { create } from '@storybook/theming';

export default create({
  base: 'light',
  brandTitle: 'Components',
  brandUrl: '',
  brandImage: 'https://logodownload.org/wp-content/uploads/2019/12/itau-bba-logo-1.png',
});